set(_src_filters
    filter.cpp
)

set(_hdr_filters

)

add_library(filters ${_src_filters} ${_hdr_filters})
target_include_directories(filters INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
target_compile_features(filters PUBLIC cxx_std_17)
target_compile_options(filters PUBLIC -Wall -Wextra)
target_link_libraries(filters PUBLIC CImg)

add_subdirectory(conv)
