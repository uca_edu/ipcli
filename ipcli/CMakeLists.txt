
set(_src_ipcli
    ipcli.cpp
)

set(_hdr_ipcli

)

add_subdirectory(filters)
add_subdirectory(cli)

if(${ENABLE_TESTS})
    add_subdirectory(tests)
endif()

add_executable(ipcli ${_src_ipcli} ${_hdr_ipcli})

target_link_libraries(ipcli PRIVATE filters cli)
target_compile_features(ipcli PRIVATE cxx_std_17)
target_compile_options(ipcli PRIVATE -Wall -Wextra)