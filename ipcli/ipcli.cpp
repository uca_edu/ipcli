#include <CImg/CImg.h>
#include <cstdint>

int main() {
    cimg_library::CImg<uint8_t> image("input.bmp");
    
    for (int y = 0; y < image.height(); ++y) {
        for (int x = 0; x < image.width(); ++x) {
            // accès en lecture et en écriture aux canaux rouge, vert et bleu de l'images
            image(x, y, 0, 0);  // Rouge
            image(x, y, 0, 1);  // Vert
            image(x, y, 0, 2);  // Bleu
        }
    }

    image.save("output.bmp");
}